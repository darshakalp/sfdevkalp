public with sharing class productLineItems {
    @AuraEnabled(cacheable=true)
    public static List<SObject> getProdLineItems(String opportunityId) {
        return  [ SELECT Id, Name, Account.Name, (SELECT Quantity, UnitPrice, TotalPrice, PricebookEntry.Name, 
                        PricebookEntry.Product2.Family FROM OpportunityLineItems) FROM Opportunity WHERE Id =: opportunityId ];
    }
}