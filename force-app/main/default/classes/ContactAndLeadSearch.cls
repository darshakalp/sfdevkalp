public class ContactAndLeadSearch {
    public static  List<List< SObject>> searchContactsAndLeads(String str){
        List<List<sObject>> searchList = [FIND :str in NAME FIELDS 
                   RETURNING Contact(Name),Lead(FirstName)];
        
        return searchList;
    }
}