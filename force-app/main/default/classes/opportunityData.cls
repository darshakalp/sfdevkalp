public with sharing class opportunityData {
    @AuraEnabled(cacheable=true)
    public static List<OpportunityLineItem> getOpportunities(String accId) {
        return  [Select Id, Product2Id, Product2.Name,Quantity, ServiceDate,UnitPrice 
        From OpportunityLineItem Where OpportunityId =: accId ];
    }
}