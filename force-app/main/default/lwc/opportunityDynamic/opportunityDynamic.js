import { LightningElement, api, wire } from 'lwc';
import getOpportunities from '@salesforce/apex/opportunityData.getOpportunities';
export default class opportunityDynamic extends LightningElement {
    @api recordId;
    @wire(getOpportunities,{accId: '$recordId'})
    opportunities;
}