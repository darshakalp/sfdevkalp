import { LightningElement, api } from 'lwc';

export default class currentDateTime extends LightningElement {
@api currentDate;
currentDate = new Date().toDateString();
}