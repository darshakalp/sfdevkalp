import { LightningElement, track } from 'lwc';


// const mainData = [
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001JamzQAC",
//       "BusinessUnit__c": "OPTICAL",
//       "ProductGroup__c": "FLASHWAVE 9500",
//       "SBQQ__Product__c": "01t2C000001mcD7QAI",
//       "SBQQ__Description__c": "Trib Shelf Lit with SF52 Switch Fabric",
//       "SBQQ__Discount__c": 0,
//       "SBQQ__Quantity__c": 5,
//       "SBQQ__ListPrice__c": 16630,
//       "SBQQ__NetPrice__c": 16630,
//       "SBQQ__NetTotal__c": 83150,
//       "SBQQ__ListTotal__c": 83150,
//       "Id": "a1k2C000003tUkCQAU",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001JamzQAC",
//         "Name": "Site 1",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "FW9500SF52T",
//         "SQT_ProductHierarchyName__c": "FLASHWAVE 9500",
//         "SAP_Product_Hierarchy__c": "10000900000000",
//         "BusinessUnit__c": "OPTICAL",
//         "SQT_Bundle_Type__c": "FNC",
//         "Id": "01t2C000001mcD7QAI"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001JamzQAC",
//       "BusinessUnit__c": "OPTICAL",
//       "ProductGroup__c": "FLASHWAVE 7120",
//       "SBQQ__Product__c": "01t2C000001mbUEQAY",
//       "SBQQ__Description__c": "FW7120 STARTER KIT (2RU Shelf w/1 10G)",
//       "SBQQ__Discount__c": 0,
//       "SBQQ__Quantity__c": 7,
//       "SBQQ__ListPrice__c": 198420,
//       "SBQQ__NetPrice__c": 198420,
//       "SBQQ__NetTotal__c": 1388940,
//       "SBQQ__ListTotal__c": 1388940,
//       "Id": "a1k2C000003tUkHQAU",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001JamzQAC",
//         "Name": "Site 1",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "FW7120M431KIT01",
//         "SQT_ProductHierarchyName__c": "FLASHWAVE 7120",
//         "SAP_Product_Hierarchy__c": "1000140000000000",
//         "BusinessUnit__c": "OPTICAL",
//         "SQT_Bundle_Type__c": "FNC",
//         "Id": "01t2C000001mbUEQAY"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001JamzQAC",
//       "BusinessUnit__c": "SOFTWARE",
//       "ProductGroup__c": "FNC SOFTWARE",
//       "SBQQ__Product__c": "01t2C000001mcvVQAQ",
//       "SBQQ__Description__c": "NETSMART 1500 v9 PM FTP MODULE LICENSE",
//       "SBQQ__Discount__c": 65,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 151,
//       "SBQQ__NetPrice__c": 52.85,
//       "SBQQ__NetTotal__c": 52.85,
//       "SBQQ__ListTotal__c": 151,
//       "Id": "a1k2C000003tUklQAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001JamzQAC",
//         "Name": "Site 1",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "NS-09-0-0-PMFTP",
//         "SQT_ProductHierarchyName__c": "NETSMART 1500",
//         "SAP_Product_Hierarchy__c": "1000100000000000",
//         "BusinessUnit__c": "SOFTWARE",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001mcvVQAQ"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001JamzQAC",
//       "BusinessUnit__c": "SOFTWARE",
//       "ProductGroup__c": "NETSMART PRODUCTS",
//       "SBQQ__Product__c": "01t2C000001mcwYQAQ",
//       "SBQQ__Description__c": "NS1500 Inventory NBI (MTOSI)",
//       "SBQQ__Discount__c": 65,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 151,
//       "SBQQ__NetPrice__c": 52.85,
//       "SBQQ__NetTotal__c": 52.85,
//       "SBQQ__ListTotal__c": 151,
//       "Id": "a1k2C000003tUkmQAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001JamzQAC",
//         "Name": "Site 1",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "NS-10-0-0-INVAPI",
//         "SQT_ProductHierarchyName__c": "NETSMART 1500",
//         "SAP_Product_Hierarchy__c": "1000100000000000",
//         "BusinessUnit__c": "SOFTWARE",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001mcwYQAQ"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001JamzQAC",
//       "BusinessUnit__c": "SOFTWARE",
//       "ProductGroup__c": "FNC SOFTWARE",
//       "SBQQ__Product__c": "01t2C000001mcujQAA",
//       "SBQQ__Description__c": "NETSMART 1500 FW4100LS or FW4100ES license",
//       "SBQQ__Discount__c": 65,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 454,
//       "SBQQ__NetPrice__c": 158.9,
//       "SBQQ__NetTotal__c": 158.9,
//       "SBQQ__ListTotal__c": 454,
//       "Id": "a1k2C000003tUknQAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001JamzQAC",
//         "Name": "Site 1",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "NS074100",
//         "SQT_ProductHierarchyName__c": "NETSMART 1500",
//         "SAP_Product_Hierarchy__c": "1000100000000000",
//         "BusinessUnit__c": "SOFTWARE",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001mcujQAA"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001JamzQAC",
//       "BusinessUnit__c": "OTHER BU",
//       "SBQQ__Product__c": "01t2C000001nJGAQA2",
//       "SBQQ__Discount__c": 45,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 0,
//       "SBQQ__NetPrice__c": 0,
//       "SBQQ__NetTotal__c": 0,
//       "SBQQ__ListTotal__c": 0,
//       "Id": "a1k2C000003tV92QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001JamzQAC",
//         "Name": "Site 1",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "CUSTOM HARDWARE 1",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001nJGAQA2"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001JamzQAC",
//       "BusinessUnit__c": "OTHER BU",
//       "SBQQ__Product__c": "01t2C000001niiGQAQ",
//       "SBQQ__Discount__c": 45,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 4000,
//       "SBQQ__NetPrice__c": 2200,
//       "SBQQ__NetTotal__c": 2200,
//       "SBQQ__ListTotal__c": 4000,
//       "Id": "a1k2C000003tV93QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001JamzQAC",
//         "Name": "Site 1",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "CUSTOM HARDWARE 5",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001niiGQAQ"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001Jbw7QAC",
//       "BusinessUnit__c": "OPTICAL",
//       "ProductGroup__c": "FLASHWAVE 9500",
//       "SBQQ__Product__c": "01t2C000001mcD7QAI",
//       "SBQQ__Description__c": "Trib Shelf Lit with SF52 Switch Fabric",
//       "SBQQ__Discount__c": 0,
//       "SBQQ__Quantity__c": 5,
//       "SBQQ__ListPrice__c": 16630,
//       "SBQQ__NetPrice__c": 16630,
//       "SBQQ__NetTotal__c": 83150,
//       "SBQQ__ListTotal__c": 83150,
//       "Id": "a1k2C000003tVd3QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001Jbw7QAC",
//         "Name": "Site 2",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "FW9500SF52T",
//         "SQT_ProductHierarchyName__c": "FLASHWAVE 9500",
//         "SAP_Product_Hierarchy__c": "10000900000000",
//         "BusinessUnit__c": "OPTICAL",
//         "SQT_Bundle_Type__c": "FNC",
//         "Id": "01t2C000001mcD7QAI"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001Jbw7QAC",
//       "BusinessUnit__c": "OPTICAL",
//       "ProductGroup__c": "FLASHWAVE 7120",
//       "SBQQ__Product__c": "01t2C000001mbUEQAY",
//       "SBQQ__Description__c": "FW7120 STARTER KIT (2RU Shelf w/1 10G)",
//       "SBQQ__Discount__c": 0,
//       "SBQQ__Quantity__c": 7,
//       "SBQQ__ListPrice__c": 198420,
//       "SBQQ__NetPrice__c": 198420,
//       "SBQQ__NetTotal__c": 1388940,
//       "SBQQ__ListTotal__c": 1388940,
//       "Id": "a1k2C000003tVd4QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001Jbw7QAC",
//         "Name": "Site 2",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "FW7120M431KIT01",
//         "SQT_ProductHierarchyName__c": "FLASHWAVE 7120",
//         "SAP_Product_Hierarchy__c": "1000140000000000",
//         "BusinessUnit__c": "OPTICAL",
//         "SQT_Bundle_Type__c": "FNC",
//         "Id": "01t2C000001mbUEQAY"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001Jbw7QAC",
//       "BusinessUnit__c": "SOFTWARE",
//       "ProductGroup__c": "FNC SOFTWARE",
//       "SBQQ__Product__c": "01t2C000001mcvVQAQ",
//       "SBQQ__Description__c": "NETSMART 1500 v9 PM FTP MODULE LICENSE",
//       "SBQQ__Discount__c": 65,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 151,
//       "SBQQ__NetPrice__c": 52.85,
//       "SBQQ__NetTotal__c": 52.85,
//       "SBQQ__ListTotal__c": 151,
//       "Id": "a1k2C000003tVd5QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001Jbw7QAC",
//         "Name": "Site 2",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "NS-09-0-0-PMFTP",
//         "SQT_ProductHierarchyName__c": "NETSMART 1500",
//         "SAP_Product_Hierarchy__c": "1000100000000000",
//         "BusinessUnit__c": "SOFTWARE",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001mcvVQAQ"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001Jbw7QAC",
//       "BusinessUnit__c": "SOFTWARE",
//       "ProductGroup__c": "NETSMART PRODUCTS",
//       "SBQQ__Product__c": "01t2C000001mcwYQAQ",
//       "SBQQ__Description__c": "NS1500 Inventory NBI (MTOSI)",
//       "SBQQ__Discount__c": 65,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 151,
//       "SBQQ__NetPrice__c": 52.85,
//       "SBQQ__NetTotal__c": 52.85,
//       "SBQQ__ListTotal__c": 151,
//       "Id": "a1k2C000003tVd6QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001Jbw7QAC",
//         "Name": "Site 2",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "NS-10-0-0-INVAPI",
//         "SQT_ProductHierarchyName__c": "NETSMART 1500",
//         "SAP_Product_Hierarchy__c": "1000100000000000",
//         "BusinessUnit__c": "SOFTWARE",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001mcwYQAQ"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001Jbw7QAC",
//       "BusinessUnit__c": "SOFTWARE",
//       "ProductGroup__c": "FNC SOFTWARE",
//       "SBQQ__Product__c": "01t2C000001mcujQAA",
//       "SBQQ__Description__c": "NETSMART 1500 FW4100LS or FW4100ES license",
//       "SBQQ__Discount__c": 65,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 454,
//       "SBQQ__NetPrice__c": 158.9,
//       "SBQQ__NetTotal__c": 158.9,
//       "SBQQ__ListTotal__c": 454,
//       "Id": "a1k2C000003tVd7QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001Jbw7QAC",
//         "Name": "Site 2",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "NS074100",
//         "SQT_ProductHierarchyName__c": "NETSMART 1500",
//         "SAP_Product_Hierarchy__c": "1000100000000000",
//         "BusinessUnit__c": "SOFTWARE",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001mcujQAA"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001Jbw7QAC",
//       "BusinessUnit__c": "OTHER BU",
//       "SBQQ__Product__c": "01t2C000001nJGAQA2",
//       "SBQQ__Discount__c": 45,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 0,
//       "SBQQ__NetPrice__c": 0,
//       "SBQQ__NetTotal__c": 0,
//       "SBQQ__ListTotal__c": 0,
//       "Id": "a1k2C000003tVd8QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001Jbw7QAC",
//         "Name": "Site 2",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "CUSTOM HARDWARE 1",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001nJGAQA2"
//       }
//     },
//     {
//       "SBQQ__Quote__c": "a1o2C000000zIRAQA2",
//       "SBQQ__Group__c": "a1i2C000001Jbw7QAC",
//       "BusinessUnit__c": "OTHER BU",
//       "SBQQ__Product__c": "01t2C000001niiGQAQ",
//       "SBQQ__Discount__c": 45,
//       "SBQQ__Quantity__c": 1,
//       "SBQQ__ListPrice__c": 4000,
//       "SBQQ__NetPrice__c": 2200,
//       "SBQQ__NetTotal__c": 2200,
//       "SBQQ__ListTotal__c": 4000,
//       "Id": "a1k2C000003tVd9QAE",
//       "SBQQ__Quote__r": {
//         "Id": "a1o2C000000zIRAQA2"
//       },
//       "SBQQ__Group__r": {
//         "Id": "a1i2C000001Jbw7QAC",
//         "Name": "Site 2",
//         "SQT_Network__c": "a2F2C000000IYfpUAG"
//       },
//       "SBQQ__Product__r": {
//         "Name": "CUSTOM HARDWARE 5",
//         "SQT_Bundle_Type__c": "CPQ",
//         "Id": "01t2C000001niiGQAQ"
//       }
//     }
//   ];

const displayCol = [
    {label: 'Product Name', fieldName: 'SBQQ__Product__c', type: 'text'},
    {label: 'Discription', fieldName: 'SBQQ__Description__c', type: 'text'},
    {label: 'Total Quantity', fieldName: 'opportunityName', type: 'number'},
]


export default class HelloWorld extends LightningElement {
    @track data;
    @track columns = displayCol;

    HelloWorld(){
        this.createDataTable();
    }

    createDataTable(){

    }
}