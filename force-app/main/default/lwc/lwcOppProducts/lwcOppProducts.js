/* eslint-disable no-case-declarations */
import { LightningElement, api, wire, track } from 'lwc';
import getProdLineItems from '@salesforce/apex/productLineItems.getProdLineItems'

const tabeldata = [
    {
        name: 'proudct1',
        price: 1000,
        quantity: 10,
    },
    {
        name: 'proudct2',
        price: 500,
        quantity: 5,
    },
    {
        name: 'proudct3',
        price: 525,
        quantity: 1,
    }
];
const columnsData = [
    { label: 'Name', fieldName: 'name', editable : 'true', type: 'text' },
    { label: 'Price', fieldName: 'price', type: 'number' },
    { label: 'Quantity', fieldName: 'quantity', type: 'number' },
    { type: 'action', typeAttributes: { 
        rowActions: [
            { label: 'Show Details', name: 'showDetails', sortable: true },
            { label: 'Delete', name: 'delete', sortable: true }
        ], 
        menuAlignment: 'center' } 
    }
];
 
export default class LwcOppProducts extends LightningElement {
    @api recordId;
    @wire(getProdLineItems,{opportunityId: '$recordId'})
    productLineItems;
    @track data = tabeldata;
    @track columns = columnsData;
    
    handleRowAction(event){
        const actionName = event.detail.action.name;
        const row = JSON.parse(JSON.stringify(event.detail.row));
        switch(actionName){
            case 'delete':
                const rows = JSON.parse(JSON.stringify(this.data));
                const rowIndex = rows.indexOf(row);
                rows.splice(rowIndex, 1);
                this.data = rows;
                break;
            case 'showDetails':
                // eslint-disable-next-line no-alert
                alert(JSON.stringify(this.data));
                break;
            default:
                
        }
    }

}