({
	handleClick : function(component, event, helper) {
		const arrayIndex = component.get('v.arrayIndex');
        const deleteEvent = $A.get('e.c:TodoDeleteTask');
        deleteEvent.setParam('arrayIndex', arrayIndex);
        deleteEvent.fire();
	}
})