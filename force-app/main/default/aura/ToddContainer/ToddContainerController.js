({
	handleDeleteTask : function(component, event, helper) {
		let tasks = component.get('v.tasks');
        const arrayIndex = event.getParam('arrayIndex');
        tasks.splice(arrayIndex, 1);
        component.set('v.tasks', tasks);
	}
    
})