({
	 init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'Billing street', fieldName: 'BillingStreet', type: 'text'}
        ]);

        var action = cmp.get("c.getAccLst");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               var data = response.getReturnValue();
               cmp.set('v.data', data);
            }
            else if (state === "ERROR") {
                
            }
        });
        
        $A.enqueueAction(action);

    }
})